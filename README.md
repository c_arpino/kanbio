# KanBio

Nós da KanBio construímos, usamos e capacitamos comunidades através do projeto Precious Plastic. Trabalhamos com inovação socioambiental foco na criação participativa de mais valor e autonomia para comunidades. Atuamos como plano de fundo pois sabemos que a comunidade é protagonista das transformações sociais. Nosso objetivo ao término de nossas ações é sair ser sermos notados.

Conheça nossa estória acessando a [wiki do projeto]()! : )


Atualmente o trabalho da KanBio tem foco na implementação de um "Workspace Precious Plastic" em comunidades de baixa renda que possuem como atividade econômica a coleta, gestão e reciclagem de resíduos. Entendemos que o "Workspace", ou container de reciclagem de plasticos não é nada mais do que uma ferramenta, que se não fizer sentido para a própria comunidade não será usada, portanto o verdadeiro valor deste trabalho esta na capacitação de agentes e lideranças sociais que possuem papel educativo de difusão do conhecimento na comunidade, para que o conhecimento do início ao fim seja construído pela comunidade e difundido de dentro para fora. Entendermos que nós como capacitadores somos apenas facilitadores que auxiliam no protagonismo dos agentes comunitários.

Em nosso trabalho destacamos 4 etapas:
- Identificaçao de Agentes e Lideranças Comunitárias
- Capacitação Educativa dos Agentes e Lideranças
- Criação de Valor e Transformação Comunitária
- Difusão do Projeto para Mais Comunidades

## Contato
- Coordenador do projeto
    - Cristthian Marafigo Arpino - kanbio@protonmail.com

## Parceiros:
- Associação Cultural Vila Flores
- Projeto Cidade Aberta (POA Inquieta)

## Apoio
- SENAC