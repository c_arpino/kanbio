## Avisos legais e licenciamento

Primeiramente pensamos em escrever um desses longos termos e condições aqui. O que é super chato de ler e escrever. Então para facilitar, fizemos uma versão curta: Tudo é gratuito para qualquer uso (comercial ou pessoal). Nós apreciaríamos se você mencionasse nosso nome em algum lugar. Ah, e você não pode nos processar se algo der errado. Por isso seja cuidadoso! Mas para aqueles que gostam de versões longas de termos e confições, podem conferir abaixo as licenças que utilizamos:

- [Precious Plastic](preciousplastic.com/) - Copyright (c) 2016 Dave Hakkens
- The MIT License (MIT)
    - ![MIT Licence](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/MIT_OSI-70x96.png)
- **Attribution ShareAlike [(CC BY-SA 4.0 Internacional)](https://creativecommons.org/licenses/by-sa/4.0/legalcode.pt)**
    - Esta obra está licenciado com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional.
    - ![CC BY-SA](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CC_BY-SA.88x31.png)

Autor: Cristthian Marafigo Arpino
